﻿<?php ob_start() ?>
<!DOCTYPE html>
<html>
<head>
 <title>เพิ่มหนังสือ</title>
 <link rel="preconnect" href="https://fonts.gstatic.com">
<link href="https://fonts.googleapis.com/css2?family=Taviraj:wght@100&display=swap" rel="stylesheet">
</head>
<body bgcolor="FFE8D2">

<tr bgcolor="#000000" align="Right" >
 <td>
  <style type="text/css" >
ul.v_menu{ /* กำหนดขอบเขตของเมนู */
	list-style:none;
	margin:0px;
	padding:0px;
	font-family: 'Taviraj', serif;
	font-size:10px;
}
ul.v_menu > table{ /* กำหนดรูปแบบให้กับเมนูหลัก */
	display:block;
	width:60%;
	height:30px;
	text-indent:5px;
	background-color:#000000;
	/*border:1px #006633 0095AA inset;*/
	border:1px #006633;
	float:left;
	text-align:center;
}
ul.v_menu > li{ /* กำหนดรูปแบบให้กับเมนูหลัก */
	display:block;
	width:10%;
	height:30px;
	text-indent:5px;
	background-color:#000000;
	/*border:1px #006633 inset;*/
	border:1px #006633;
	float:left;
	text-align:center;
}
ul.v_menu > li:hover{ /* กำหนดรูปแบบให้กับเมนูเมื่อมีเมาส์อยู่เหนือ */
	display:block;
	width:10%;
	height:30px;
	text-indent:5px;
	background-color:#808080;
	/*border:1px #006633 inset;*/
	border:1px #006633;
	float:left;
	text-align:center;	
}
ul.v_menu > li > a,ul.v_menu > li > ul > li > a{ /* กำหนดรูปแบบให้กับลิ้งค์ */
	text-decoration:none;
	/*color:#666666;*/
	color: #FFFFFF;		
	line-height:25px;
}
ul.v_menu > li > ul{ 
	display:none;
	list-style:none;
	margin:0px;
	padding:0px;
}
ul.v_menu > li:hover > ul { 
	display:block;
	width:10%;
}	
ul.v_menu > li > ul > li{ /* กำหนดรูปแบบให้กับเมนูย่อย */
	display:block;
	width:190px;
	height:30px;
	text-indent:5px;
	background-color:#000000;
	border:1px #F4F4F4 solid;
	float:left;
	text-align:left;
}
ul.v_menu > li > ul > li:hover{ /* กำหนดรูปแบบให้กับเมนูย่อยเมื่อเมาส์อยู่เหนือ */
	display:block;
	width:190px;
	height:30px;
	text-indent:5px;
	background-color:#808080;
	border:1px #F4F4F4 solid;
	float:left;
	text-align:left;
}
</style>


<table bgcolor="F08879" width=166%  ><tr>
 <td>
    <a href="book_stock.php"><img src="img/logo1.jpg" width=200px > </a>
 </td>
 <td >
	<th width=46%><ul class="v_menu" ><li><a href="login.php" >ออกจากระบบ</a></li></ul></th>
 </td>
</tr></table>


<ul class="v_menu" width="100%">

	<table></table>
	<li><a href="book_stock.php">สต็อกหนังสือ</a></li>	
	<li><a href="book_insert.php">เพิ่มหนังสือ</a></li>			
	<li><a href="order_admin.php">รายการสั่งซื้อ</a></li>
	<li><a href="#">สรุปยอดขาย</a><ul>
			<li><a href="sum_month.php?txtfindmonth">รายเดือน </a></li>
			<li><a href="sum_year.php?search">รายปี</a></li>
		</ul>	
	</li>
</ul>  </td>
</tr>
<br>
<br>

<h1 align="center">รายละเอียดหนังสือ</h1>

<form method="POST" align="center" >
<table align="center">
	<tr><th>ชื่อหนังสือ</th><td><input type="text" name="b_name" value="" required></td></tr>
	<tr><th>รูปหนังสือ</th><td><input type="file" name="b_pic"></td></tr>
	<tr><th>ชื่อผู้แต่ง</th><td><input type="text" name="b_author" value="" required></td></tr>
	<tr><th>ปีที่พิมพ์</th><td><input type="text" name="b_year" value="" onkeypress='return event.charCode >= 48 && event.charCode <= 57' ></td></tr>
	<tr><th>ประเภทหนังสือ</th><td><select name="b_type" align="left" cols="22" rows="1" >
		<option value="">เลือก</option>
		<option value="ทั่วไป" >ทั่วไป</option>
		<option value="สารคดี" >สารคดี</option>
		<option value="นวนิยาย" >นวนิยาย</option>
		<option value="การ์ตูน"> การ์ตูน</option>
		<option value="วารสาร">วารสาร</option></select></td></tr>
	<tr><th>รายละเอียดหนังสือ</th><td><textarea name="b_detail" value=" " cols="22" rows="1" ></textarea></td></tr>
	<tr><th>ราคา(บาท)</th><td><input type="text" name="b_price" value="" onkeypress='return event.charCode >= 48 && event.charCode <= 57' ></td></tr>
	
</table><br>
<button type="submit" name="done" >เพิ่มข้อมูล</button>
</form>

<?php include 'connect_database.php';
if(isset($_POST['done'])){
	$b_name = $_POST['b_name'];
	$b_pic = $_POST['b_pic'];
	$b_author = $_POST['b_author'];
	$b_year = $_POST['b_year'];
	$b_type = $_POST['b_type'];
	$b_detail = $_POST['b_detail'];
	$b_price = $_POST['b_price'];
	
	$sql = "INSERT INTO book ( b_name , b_pic , b_author , b_year , b_type , b_detail , b_price ) VALUE ('$b_name', '$b_pic' , '$b_author' , '$b_year' , '$b_type' , '$b_detail' , '$b_price'  )";
	echo ($sql);
	
	$query = mysqli_query($conn, $sql);
	header('location:book_stock.php');
}
?>


</body>
</html>
<?php ob_end_flush() ?>