-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 01, 2021 at 10:13 AM
-- Server version: 10.4.17-MariaDB
-- PHP Version: 8.0.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `snnbook`
--

-- --------------------------------------------------------

--
-- Table structure for table `book`
--

CREATE TABLE `book` (
  `b_id` int(10) NOT NULL,
  `b_pic` varchar(50) NOT NULL,
  `b_name` varchar(50) NOT NULL,
  `b_year` char(4) NOT NULL,
  `b_author` varchar(50) NOT NULL,
  `b_type` varchar(50) NOT NULL,
  `b_detail` varchar(255) NOT NULL,
  `b_price` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `book`
--

INSERT INTO `book` (`b_id`, `b_pic`, `b_name`, `b_year`, `b_author`, `b_type`, `b_detail`, `b_price`) VALUES
(100, 'nor1.jpg', '7 สิ่งมหัศจรรย์ของโลก', '2016', 'กองบรรณาธิการเอ็มไอเอส', 'หนังสือทั่วไป', ' นำเสนอเรื่องราวน่าสนใจของสิ่งมหัศจรรย์ของโลก ตั้งแต่ยุคแรกเริ่มของโลก', 53),
(101, 'nor2.jpg', 'กินแหลกอร่อย 500 ร้านเด็ด', '2008', 'DPlus Guide Team', 'หนังสือทั่วไป', 'แนะนำร้านอร่อยให้กับผู้ที่ชื่นชอบได้ลิ้มลอง', 255),
(102, 'nor3.jpg', '70 ความรู้ กินอยู่ให้ถูกวิธี', '2015', '-', 'หนังสือทั่วไป', 'เรียนรู้การกินให้ถูกวิธี', 69),
(200, 'nov1.jpg', 'อกเกือบหัก แอบรักคุณสามี', '2017', 'นาวาร้อยกวี', 'นวนิยาย', 'เหมย จะทำอย่างไรให้พี่เทียรสนใจ มาติดตามได้ในเล่มนี้เลย', 395),
(201, 'nov2.jpg', 'พ่อมด เจ้าเสน่ห์', '2014', 'โสภี พรรณราย', 'นวนิยาย', '-', 335),
(202, 'nov3.jpg', 'ยุทธการปราบนางมาร', '2019', 'ดาริส ', 'นวนิยาย', '-', 325),
(300, 'va1.jpg', 'วารสารธรรมชาติและสิ่งแวดล้อม', '2563', '-', 'วารสาร', '-', 235),
(301, 'va2.jpg', 'ศปทส.ตร.', '2563', '-', 'วารสาร', '-', 450),
(302, 'va3.jpg', 'วารสารวิทยาศาสตร์และวิทยาศาสตร์ศึกษา ISSN', '2564', '-', 'วารสาร', '-', 432),
(303, 'ni1.jpg', 'Elle Magazine(นิตยสารแอล)', '2019', '-', 'วารสาร', 'แฟชั่นรายเดือน', 567),
(400, 'ani1.jpg', 'เอาชีวิตรอดในโลกของสัตว์', '-', 'Gomdori co.(กอมโดริ คัมพานี)', 'การ์ตูน', '-', 158),
(401, 'ani2.jpg', 'เอาชีวิตรอดจากเมืองใต้ดิน', '-', 'Sweet Factory (สวีตแฟกตอรี)', 'การ์ตูน', '-', 150),
(402, 'ani3.jpg', 'วันพีช', '-', 'เออิจิโระ โอดะ', 'การ์ตูน', '-', 70),
(500, 'doc1.jpg', 'กวางผามหัศจรรย์สี่ขา แห่งผาสูง', '2018', 'เจ้าชายน้อย', 'สารคดี', '-', 125),
(501, 'doc2.jpg', 'ไดโนเสาร์พันธุ์ไทย', '2019', '-', 'สารคดี', '-', 543);

-- --------------------------------------------------------

--
-- Table structure for table `month`
--

CREATE TABLE `month` (
  `m_id` int(3) NOT NULL,
  `m_name` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `month`
--

INSERT INTO `month` (`m_id`, `m_name`) VALUES
(1, 'January'),
(2, 'February'),
(3, 'March'),
(4, 'April'),
(5, 'May'),
(6, 'June'),
(7, 'July'),
(8, 'August'),
(9, 'September'),
(10, 'October'),
(11, 'November'),
(12, 'December');

-- --------------------------------------------------------

--
-- Table structure for table `ordering`
--

CREATE TABLE `ordering` (
  `o_id` int(15) NOT NULL,
  `o_status` varchar(11) DEFAULT NULL,
  `o_slip` text DEFAULT NULL,
  `o_price` float DEFAULT NULL,
  `o_date` char(8) DEFAULT NULL,
  `o_month` int(3) DEFAULT NULL,
  `o_year` char(4) DEFAULT NULL,
  `u_user` varchar(10) DEFAULT NULL,
  `b_id` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ordering`
--

INSERT INTO `ordering` (`o_id`, `o_status`, `o_slip`, `o_price`, `o_date`, `o_month`, `o_year`, `u_user`, `b_id`) VALUES
(10, 'รับที่ร้าน', 'user.png', 256, '12', 2, '2553', 'pam', '33'),
(101, 'จัดส่ง', 'user.png', 256, '12', 2, '2553', 'pam', '32'),
(102, 'จัดส่ง', '', NULL, '21 มีนาค', 3, '2564', 'sofiya', '11'),
(103, 'รับที่ร้าน', '163181345_937027226836201_491441010791416318_n.jpg', NULL, '23', 1, '2560', 'sofiya', '87'),
(104, 'รับที่ร้าน', '163181345_937027226836201_491441010791416318_n.jpg', NULL, '1', 4, '2564', 'sofiya', '89'),
(105, 'จัดส่ง', '163181345_937027226836201_491441010791416318_n.jpg', NULL, '21', 3, '2564', 'hasenah', '87'),
(106, 'จัดส่ง', '', NULL, '15', 11, '2564', 'sofiya', '18'),
(107, 'รับที่ร้าน', '', NULL, '6', 4, '2564', 'tiyada', '95'),
(108, 'จัดส่ง', '', NULL, '8', 10, '2564', 'tiyada', '89'),
(109, 'จัดส่ง', '', NULL, '8', 2, '2564', 'yaimai', '18'),
(110, 'จัดส่ง', '', NULL, '8', 2, '2564', 'yaimai', '18'),
(111, 'รับที่ร้าน', '', NULL, '29', 3, '2564', 'sofiya', '86'),
(112, 'รับที่ร้าน', '', NULL, '1', 3, '2564', 'zho', '100'),
(113, 'จัดส่ง', '', NULL, '8', 1, '2563', 'zho', '301'),
(114, 'รับที่ร้าน', '', NULL, '23', 2, '2564', 'zho', '401'),
(115, 'จัดส่ง', '', NULL, '25', 5, '2563', 'zho', '200'),
(116, 'รับที่ร้าน', '', NULL, '21', 5, '2560', 'nur', '202');

-- --------------------------------------------------------

--
-- Table structure for table `type`
--

CREATE TABLE `type` (
  `t_id` char(2) NOT NULL,
  `t_name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `type`
--

INSERT INTO `type` (`t_id`, `t_name`) VALUES
('1', 'user'),
('2', 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `u_user` varchar(10) NOT NULL,
  `u_pwd` varchar(50) NOT NULL,
  `u_name` varchar(50) DEFAULT NULL,
  `u_surname` varchar(50) DEFAULT NULL,
  `u_address` varchar(255) DEFAULT NULL,
  `u_phone` char(10) NOT NULL,
  `t_id` char(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`u_user`, `u_pwd`, `u_name`, `u_surname`, `u_address`, `u_phone`, `t_id`) VALUES
('admin', '123456', 'nameadmin', 'surname', 'ร้านหนังสือ SNN BOOK', '0987654321', '2'),
('nur', '123', 'ธิญาดา', 'ช่วยการกล้า', '59 หมู่ 5 ต.บางจาก อ.ด่านสุสาน จ.เลย 69854 ', '0812345698', '1'),
('zho', '12345', 'โซฟีหย๊ะ', 'บิลแสละ', '79/2 ม.16 ต.ท่าช้าง อ.บางกล่ำ จ.สงขลา 90110', '0873977236', '1');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `book`
--
ALTER TABLE `book`
  ADD PRIMARY KEY (`b_id`);

--
-- Indexes for table `month`
--
ALTER TABLE `month`
  ADD PRIMARY KEY (`m_id`);

--
-- Indexes for table `ordering`
--
ALTER TABLE `ordering`
  ADD PRIMARY KEY (`o_id`),
  ADD KEY `foreign key u_user` (`u_user`),
  ADD KEY `foreign key b_id` (`b_id`);

--
-- Indexes for table `type`
--
ALTER TABLE `type`
  ADD PRIMARY KEY (`t_id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`u_user`),
  ADD KEY `foreign key t_id` (`t_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `book`
--
ALTER TABLE `book`
  MODIFY `b_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=502;

--
-- AUTO_INCREMENT for table `ordering`
--
ALTER TABLE `ordering`
  MODIFY `o_id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=117;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
