<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
	<link rel="preconnect" href="https://fonts.gstatic.com">
	<link href="https://fonts.googleapis.com/css2?family=Taviraj:wght@100&display=swap" rel="stylesheet">

    <title>Hello, Book!</title>
</head>

<body style="background-color:#FFE8D2">
	<div class="container mt-6">
		<div class="jumbotron" style="background-color:#F08879">
			<Font face='Taviraj', serif;>
				<center>
					<h1 style="color:#FFFFFF"> SNN-BOOK </h1>
				</center>
		</div>
	</div>

	<form action="" method="GET">
		<div class="row">
			<div class="col-sm-3">
				<center> ค้นหา : <input type="text" name="search" placeholder="กรอกชื่อหนังสือ"> <input type="submit" name="submit" value="ยืนยัน"> </center>
			</div>
			<div class="col">
				<link rel="stylesheet" type="text/css" href="styles.css">
				<div class="dropdown">
					<button class="dropbtn">ประเภทหนังสือ</button>
					<div class="dropdown-content">
						<a href="book_general_user.php">หมวดทั่วไป</a>
						<a href="book_cartoon_user.php">หมวดการ์ตูน</a>
						<a href="book_documentary_user.php">หมวดสารคดี</a>
						<a href="book_magazine_user.php">หมวดวารสาร</a>
						<a href="book_novel_user.php">หมวดนวนิยาย</a>
					</div>
				</div>
	</form>
	</div>
	<div class="col">
		<center> <button class="btn"><a href="member_insert.php">สมัครสมาชิก</a></button></center>
	</div>
	<div class="col">
		<center> <button class="btn"><a href="login.php">เข้าสู่ระบบ</a></button></center>
	</div>
	<div class="col">
		<center> <button class="btn"><a href="context_host.php">ติดต่อเรา</a></button></center>
	</div>
	</div>

	<br><br>

    <table align="center" border="0" width="80%">
		<tr align="center">
			<th></th>
			<th>ชื่อหนังสือ</th>
			<th>ผู้แต่ง</th>
			<th>ประเภทหนังสือ</th>
			<th>ราคา</th>
			<th></th>
			<th></th>
		</tr>
		</teble>

        <?php
        include "connect_database.php";
        $name = "ทั่วไป";
        if (isset($_GET['submit'])) {
            $search = $_GET['search'];
        } else {
            $search = "";
        }
        $sql = "SELECT * FROM Book where b_name like '%" . $search . "%' and b_type like '%" . $name . "%'";

        $query = mysqli_query($conn, $sql);
        while ($res = mysqli_fetch_array($query)) {
        ?>

            <tr class="text-center">
                <td> <img src="img/<?php echo $res['b_pic']; ?>" width="200" height="200"> </td>
                <td> <?php echo $res['b_name']; ?> </td>
                <td> <?php echo $res['b_author']; ?> </td>
                <td> <?php echo $res['b_type']; ?> </td>
                <td> <?php echo $res['b_price']; ?> บาท </td>
                <td> <button class="btn-primary btn"> <a href="book_detail_user.php?id=<?php echo $res['b_id']; ?>" class="text-white"> ดูรายละเอียด</a> </button> </td>

            </tr>
        <?php
        }
        ?>



</body>

</html>