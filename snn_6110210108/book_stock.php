﻿<!DOCTYPE html>
<html>
<head>
 <title>สต็อกหนังสือ</title>
	<link rel="preconnect" href="https://fonts.gstatic.com">
	<link href="https://fonts.googleapis.com/css2?family=Taviraj:wght@100&display=swap" rel="stylesheet">
</head>
<body bgcolor="FFE8D2"> 

<tr bgcolor="black" align="Right" >
 <td>
  <style type="text/css" >
ul.v_menu{ /* กำหนดขอบเขตของเมนู */
	list-style:none;
	margin:0px;
	padding:0px;
	font-family: 'Taviraj', serif;
	font-size:10px;
}
ul.v_menu > table{ /* กำหนดรูปแบบให้กับเมนูหลัก */
	display:block;
	width:60%;
	height:30px;
	text-indent:5px;
	background-color:black;
	/*border:1px #006633 0095AA inset;*/
	border:1px #006633;
	float:left;
	text-align:center;
}
ul.v_menu > li{ /* กำหนดรูปแบบให้กับเมนูหลัก */
	display:block;
	width:10%;
	height:30px;
	text-indent:5px;
	background-color:black;
	/*border:1px #006633 inset;*/
	border:1px #006633;
	float:left;
	text-align:center;
}
ul.v_menu > li:hover{ /* กำหนดรูปแบบให้กับเมนูเมื่อมีเมาส์อยู่เหนือ */
	display:block;
	width:10%;
	height:30px;
	text-indent:5px;
	background-color:#808080;
	/*border:1px #006633 inset;*/
	border:1px #006633;
	float:left;
	text-align:center;	
}
ul.v_menu > li > a,ul.v_menu > li > ul > li > a{ /* กำหนดรูปแบบให้กับลิ้งค์ */
	text-decoration:none;
	/*color:#666666;*/
	color: #FFFFFF;		
	line-height:25px;
}
ul.v_menu > li > ul{ 
	display:none;
	list-style:none;
	margin:0px;
	padding:0px;
}
ul.v_menu > li:hover > ul { 
	display:block;
	width:10%;
}	
ul.v_menu > li > ul > li{ /* กำหนดรูปแบบให้กับเมนูย่อย */
	display:block;
	width:190px;
	height:30px;
	text-indent:5px;
	background-color:black;
	border:1px #F4F4F4 solid;
	float:left;
	text-align:left;
}
ul.v_menu > li > ul > li:hover{ /* กำหนดรูปแบบให้กับเมนูย่อยเมื่อเมาส์อยู่เหนือ */
	display:block;
	width:190px;
	height:30px;
	text-indent:5px;
	background-color:#808080;
	border:1px #F4F4F4 solid;
	float:left;
	text-align:left;
}
</style>


<table bgcolor="F08879" width=166%  ><tr>
 <td>
    <a href="book_stock.php"><img src="img/logo1.jpg" width=200px > </a>
 </td>
 <td >
	<th width=46%><ul class="v_menu" ><li><a href="index.php" >ออกจากระบบ</a></li></ul></th>
 </td>
</tr></table>


<ul class="v_menu" width="100%">

	<table></table>
	<li><a href="book_stock.php">สต็อกหนังสือ</a></li>	
	<li><a href="book_insert.php">เพิ่มหนังสือ</a></li>			
	<li><a href="order_admin.php">รายการสั่งซื้อ</a></li>
	<li><a href="#">สรุปยอดขาย</a><ul>
			<li><a href="sum_month.php?txtfindmonth">รายเดือน </a></li>
			<li><a href="sum_year.php?search">รายปี</a></li>
		</ul>	
	</li>
</ul>  </td>
</tr>
<br>
<br>
<h1 align="center">สต็อกหนังสือ</h1>
<table bgcolor="FEAFC3" border="0" align="center">
  <tr >
 <th width=150px> รหัสหนังสือ </th>
 <th width=400px> ชื่อหนังสือ </th>
 <th width=150px> จัดการข้อมูล </th>
 <th width=150px> ลบข้อมูล </th>
 </tr >
 <?php
include "connect_database.php";
$sql = "SELECT * FROM Book";
$query = mysqli_query($conn,$sql);
while($res = mysqli_fetch_array($query)){

 ?>
 
 <tr class="text-center" >
 <td align="center"> <?php echo $res['b_id']; ?></td>
 <td align="center"> <?php echo $res['b_name']; ?></td>
 <td align="center"><a href="book_update.php?id=<?php echo $res['b_id']; ?>" class="text-white"> แก้ไข </a></td>
 <td align="center"> <a href="book_delete.php?id=<?php echo $res['b_id']; ?>" onclick="return confirm('คุณต้องการที่จะลบข้อมูลหนังสือเล่มนี้ใช่หรือไม่ ?');" class="text-white"> ลบ </a></td>
 </tr>


 <?php 
}
 ?>
  </table>  
</body>
</html>