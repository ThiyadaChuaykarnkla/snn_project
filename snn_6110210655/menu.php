<!DOCTYPE html>
<html>
<head>
 <title></title>
	<link rel="preconnect" href="https://fonts.gstatic.com
">
	<link href="https://fonts.googleapis.com/css2?family=Taviraj:wght@100&display=swap
" rel="stylesheet">
</head>
<body>
<marquee direction="right">ร้านขายหนังสือ SNN Project</marquee> 
</body>
<style type="text/css" >
ul.v_menu{ /* กำหนดขอบเขตของเมนู */
	list-style:none;
	margin:0px;
	padding:0px;
	font-family: 'Taviraj', serif;
	font-size:10px;
}
ul.v_menu > table{ /* กำหนดรูปแบบให้กับเมนูหลัก */
	display:block;
	width:60%;
	height:30px;
	text-indent:5px;
	background-color:#000000;
	/*border:1px #006633 0095AA inset;*/
	border:1px #006633;
	float:left;
	text-align:center;
}
ul.v_menu > li{ /* กำหนดรูปแบบให้กับเมนูหลัก */
	display:block;
	width:10%;
	height:30px;
	text-indent:5px;
	background-color:#000000;
	/*border:1px #006633 inset;*/
	border:1px #006633;
	float:left;
	text-align:center;
}
ul.v_menu > li:hover{ /* กำหนดรูปแบบให้กับเมนูเมื่อมีเมาส์อยู่เหนือ */
	display:block;
	width:10%;
	height:30px;
	text-indent:5px;
	background-color:#808080;
	/*border:1px #006633 inset;*/
	border:1px #006633;
	float:left;
	text-align:center;	
}
ul.v_menu > li > a,ul.v_menu > li > ul > li > a{ /* กำหนดรูปแบบให้กับลิ้งค์ */
	text-decoration:none;
	/*color:#666666;*/
	color: #FFFFFF;		
	line-height:25px;
}
ul.v_menu > li > ul{ 
	display:none;
	list-style:none;
	margin:0px;
	padding:0px;
}
ul.v_menu > li:hover > ul { 
	display:block;
	width:10%;
}	
ul.v_menu > li > ul > li{ /* กำหนดรูปแบบให้กับเมนูย่อย */
	display:block;
	width:190px;
	height:30px;
	text-indent:5px;
	background-color:#000000;
	border:1px #F4F4F4 solid;
	float:left;
	text-align:left;
}
ul.v_menu > li > ul > li:hover{ /* กำหนดรูปแบบให้กับเมนูย่อยเมื่อเมาส์อยู่เหนือ */
	display:block;
	width:190px;
	height:30px;
	text-indent:5px;
	background-color:#808080;
	border:1px #F4F4F4 solid;
	float:left;
	text-align:left;
}
</style>


<table bgcolor="F08879" width=166%  ><tr>
 <td>
    <a href="book_stock.php"><img src="img/logo1.jpg" width=200px > </a>
 </td>
 <td >
	<th width=45%><ul class="v_menu" ><li><a href="login.php" >ออกจากระบบ</a></li></ul></th>
 </td>
</tr></table>


<ul class="v_menu" width="100%">

	<table></table>
	<li><a href="book_stock.php">สต็อกหนังสือ</a></li>	
	<li><a href="book_insert.php">เพิ่มหนังสือ</a></li>			
	<li><a href="order_admin.php">รายการสั่งซื้อ</a></li>
	<li><a href="#">สรุปยอดขาย</a><ul>
			<li><a href="sum_month.php?txtfindmonth">รายเดือน </a></li>
			<li><a href="sum_year.php?search">รายปี</a></li>
		</ul>	
	</li>
</ul>