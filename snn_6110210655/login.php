<!doctype html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">

  <!-- Bootstrap CSS -->
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
  <link rel="preconnect" href="https://fonts.gstatic.com">
	<link href="https://fonts.googleapis.com/css2?family=Taviraj:wght@100&display=swap" rel="stylesheet">

  <title>Login</title>
</head>

<body style="background-color:#FFE8D2">
 <div class="container">
        <div class="jumbotron" style="background-color:#F08879">
            <Font face='Taviraj', serif;">
                <center>
                    <h1 style="color:#FFFFFF"> SNN-BOOK </h1>
                </center>
        </div>
    </div>

    <form action="" method="GET">
		<div class="row">
			<div class="col-sm-4">
				<center> ค้นหา : <input type="text" name="search" placeholder="กรอกชื่อหนังสือ"> <input type="submit" name="submit" value="ยืนยัน"> </center>
			</div>
			<div class="col">
				<link rel="stylesheet" type="text/css" href="styles.css">
				<div class="dropdown">
					<button class="dropbtn">ประเภทหนังสือ</button>
					<div class="dropdown-content">
						<a href="book_general_user.php">หมวดทั่วไป</a>
						<a href="book_cartoon_user.php">หมวดการ์ตูน</a>
						<a href="book_documentary_user.php">หมวดสารคดี</a>
						<a href="book_magazine_user.php">หมวดวารสาร</a>
						<a href="book_novel_user.php">หมวดนวนิยาย</a>
					</div>
				</div>
	</form>
	</div>
	
	<div class="col">
		<center> <button class="btn"><a href="member_insert.php">สมัครสมาชิก</a></button></center>
	</div>
	<div class="col">
		<center> <button class="btn"><a href="context_host.php">ติดต่อเรา</a></button></center>
	</div>
	</div>
   
  <br><br>
  <center>
    <img src="book.jpg">
    <form method="POST" action="checklog2.php">
      
      <br><br>
        <p> ชื่อผู้ใช้ :
          <input type="text" required name="Username" placeholder="Username">
        </p>
        <p>รหัสผ่าน :
          <input type="password" required name="Password" placeholder="Password">
        </p>
        <p>
          <button type="submit" name="submit">เข้าสู่ระบบ</button>
          &nbsp;&nbsp;

          <br>
        </p>
    </form>
  </center>

</body>

</html>