<!DOCTYPE html>
<html>
<head>
<!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
	<link rel="preconnect" href="https://fonts.gstatic.com">
	<link href="https://fonts.googleapis.com/css2?family=Taviraj:wght@100&display=swap" rel="stylesheet">

    <title>สมัครสมาชิก</title>
</head>

<body style="background-color:#FFE8D2">
 <div class="container">
        <div class="jumbotron" style="background-color:#F08879">
            <Font face='Taviraj', serif;>
                <center>
                    <h1 style="color:#FFFFFF"> SNN-BOOK </h1>
                </center>
        </div>
    </div>

    <form action="" method="GET">
		<div class="row">
			<div class="col-sm-4">
				<center> ค้นหา : <input type="text" name="search" placeholder="กรอกชื่อหนังสือ"> <input type="submit" name="submit" value="ยืนยัน"> </center>
			</div>
			<div class="col">
				<link rel="stylesheet" type="text/css" href="styles.css">
				<div class="dropdown">
					<button class="dropbtn">ประเภทหนังสือ</button>
					<div class="dropdown-content">
						<a href="book_general_user.php">หมวดทั่วไป</a>
						<a href="book_cartoon_user.php">หมวดการ์ตูน</a>
						<a href="book_documentary_user.php">หมวดสารคดี</a>
						<a href="book_magazine_user.php">หมวดวารสาร</a>
						<a href="book_novel_user.php">หมวดนวนิยาย</a>
					</div>
				</div>
	</form>
	</div>
	
	<div class="col">
		<center> <button class="btn"><a href="login.php">เข้าสู่ระบบ</a></button></center>
	</div>
	<div class="col">
		<center> <button class="btn"><a href="context_host.php">ติดต่อเรา</a></button></center>
	</div>
	</div>

    <br><br>
 <form action="" method="POST">

        <div class="row" >  <center>
            <div class="col-sm-6">
               <table>
	 <tr><th>ชื่อผู้ใช้</th><td><input type="text" name="u_user" value="" placeholder=""></td></tr>
    <tr><th>รหัสผ่าน</th><td><input type="text" name="u_pwd" value="" placeholder=""></td></tr>
	<tr><th>ชื่อ</th><td><input type="text" name="u_name" value="" required placeholder=""></td></tr>
	<tr><th>นามสกุล</th><td><input type="text" name="u_surname" value="" required  placeholder=""></td></tr>

	<tr><th>ที่อยู่</th><td><input type="text" name="u_address" value=""  placeholder=""></td></tr>


	<tr><th>เบอร์โทร</th><td><input type="text" name="u_phone" value="" maxlength="10" placeholder=""></td></tr>

	
	<tr><th>ประเภทผู้ใช้</th><td><select name="t_id">
		<option value="">เลือก</option>
		<option value="1" >สมาชิก</option>
   
				</table>
            </div>
			<br>
            <button type="submit" name="done" >ลงทะเบียน</button>
    </form>




<br>
 

<?php include 'connect_database.php';
if(isset($_POST['done'])){
    $u_user = $_POST['u_user'];
	$u_pwd = $_POST['u_pwd'];
    $u_name = $_POST['u_name'];
	$u_surname = $_POST['u_surname'];
	$u_phone = $_POST['u_phone']; 
    $u_address = $_POST['u_address'];
    $t_id = $_POST['t_id'];
	$sql = "INSERT INTO User ( u_user , u_pwd, u_name , u_surname , u_phone, u_address,t_id ) VALUE ('$u_user','$u_pwd','$u_name','$u_surname','$u_phone','$u_address','$t_id')";
    $query = mysqli_query($conn, $sql);
    header('location:login.php');
    
};
?>
</center>
</body>
</html>