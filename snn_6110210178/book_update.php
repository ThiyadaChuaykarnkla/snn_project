﻿<?php ob_start() ?>
<!DOCTYPE html>
<html>
<head>
 <title>แก้ไขข้อมูลหนังสือ</title>
 <link rel="preconnect" href="https://fonts.gstatic.com">
 <link href="https://fonts.googleapis.com/css2?family=Taviraj:wght@100&display=swap" rel="stylesheet">
 <script src="https://code.jquery.com/jquery-3.5.1.js" integrity="sha256-QWo7LDvxbWT2tbbQ97B53yJnYU3WhH/C8ycbRAkjPDc=" crossorigin="anonymous"></script>
</head>
<body bgcolor="FFE8D2">


<tr bgcolor="#000000" align="Right" >
 <td>
  <style type="text/css" >
ul.v_menu{ /* กำหนดขอบเขตของเมนู */
	list-style:none;
	margin:0px;
	padding:0px;
	font-family: 'Taviraj', serif;
	font-size:10px;
}
ul.v_menu > table{ /* กำหนดรูปแบบให้กับเมนูหลัก */
	display:block;
	width:60%;
	height:30px;
	text-indent:5px;
	background-color:#000000;
	/*border:1px #006633 0095AA inset;*/
	border:1px #006633;
	float:left;
	text-align:center;
}
ul.v_menu > li{ /* กำหนดรูปแบบให้กับเมนูหลัก */
	display:block;
	width:10%;
	height:30px;
	text-indent:5px;
	background-color:#000000;
	/*border:1px #006633 inset;*/
	border:1px #006633;
	float:left;
	text-align:center;
}
ul.v_menu > li:hover{ /* กำหนดรูปแบบให้กับเมนูเมื่อมีเมาส์อยู่เหนือ */
	display:block;
	width:10%;
	height:30px;
	text-indent:5px;
	background-color:#808080;
	/*border:1px #006633 inset;*/
	border:1px #006633;
	float:left;
	text-align:center;	
}
ul.v_menu > li > a,ul.v_menu > li > ul > li > a{ /* กำหนดรูปแบบให้กับลิ้งค์ */
	text-decoration:none;
	/*color:#666666;*/
	color: #FFFFFF;		
	line-height:25px;
}
ul.v_menu > li > ul{ 
	display:none;
	list-style:none;
	margin:0px;
	padding:0px;
}
ul.v_menu > li:hover > ul { 
	display:block;
	width:10%;
}	
ul.v_menu > li > ul > li{ /* กำหนดรูปแบบให้กับเมนูย่อย */
	display:block;
	width:190px;
	height:30px;
	text-indent:5px;
	background-color:#000000;
	border:1px #F4F4F4 solid;
	float:left;
	text-align:left;
}
ul.v_menu > li > ul > li:hover{ /* กำหนดรูปแบบให้กับเมนูย่อยเมื่อเมาส์อยู่เหนือ */
	display:block;
	width:190px;
	height:30px;
	text-indent:5px;
	background-color:#808080;
	border:1px #F4F4F4 solid;
	float:left;
	text-align:left;
}
</style>


<table bgcolor="F08879" width=166%  ><tr>
 <td>
    <a href="book_stock.php"><img src="img/logo1.jpg" width=200px > </a>
 </td>
 <td >
	<th width=45%><ul class="v_menu" ><li><a href="login.php" >ออกจากระบบ</a></li></ul></th>
 </td>
</tr></table>


<ul class="v_menu" width="100%">

	<table></table>
	<li><a href="book_stock.php">สต็อกหนังสือ</a></li>	
	<li><a href="book_insert.php">เพิ่มหนังสือ</a></li>			
	<li><a href="order_admin.php">รายการสั่งซื้อ</a></li>
	<li><a href="sum_sale.php">สรุปยอดขาย</a><ul>
			<li><a href="sum_month.php">รายเดือน </a></li>
			<li><a href="sum_year.php">รายปี</a></li>
		</ul>	
	</li>
</ul>  </td>
</tr>
<br>
<br>

<h1 align="center">แก้ไขข้อมูลหนังสือ</h1>
   
<?php 
include "connect_database.php";
$id = $_GET['id'];
$sql = "select * from Book where b_id = $id";
$query = mysqli_query($conn,$sql);
$res = mysqli_fetch_array($query);

?>
<form  method="POST" align="center">
 <table align="center">
 <tr>
    <th >รหัสหนังสือ</th>
    <td><input type="text" name="nid" value="<?php echo $res['b_id']; ?>" required ></td>
    </tr>
  <tr>
    <th >ชื่อหนังสือ</th>
    <td><input type="text" name="name" value="<?php echo $res['b_name']; ?>" required ></td>
    </tr>
  <tr>
    <th >รูปหนังสือ</th>
    <td><input type="file" name="pic" value="<?php echo $res['b_pic']; ?>" required ></td>
    </tr>
  <tr>
    <th >ผู้แต่ง</th>
    <td><input type="text" name="author" value="<?php echo $res['b_author']; ?>" required ></td>
    </tr>
  <tr>
    <th >ปีที่พิมพ์</th>
    <td><input type="text" name="year" value="<?php echo $res['b_year']; ?>" onkeypress='return event.charCode >= 48 && event.charCode <= 57' required ></td>
    </tr>
  <tr>
    <th >ประเภทหนังสือ</th>
    <td>
    <select id="myselect"  name="type" required>
    <option value="">เลือก</option>
	<option value="ทั่วไป">ทั่วไป</option>
	<option value="สารคดี">สารคดี</option>
    <option value="วารสาร">วารสาร</option>
    <option value="นวนิยาย">นวนิยาย</option>
    <option value="การ์ตูน">การ์ตูน</option>
    </select></td>
    <script> $( "#myselect" ).val("<?php echo $res['b_type']; ?>");</script>
    </tr>
  <tr>
    <th >รายละเอียดหนังสือ</th>
    <td><textarea name="detail" value="<?php echo $res['b_detail']; ?>" required  cols="22" rows="1" ></textarea></td>
    </tr>
  <tr>
    <th >ราคา(บาท)</th>
    <td><input type="text" name="price" value="<?php echo $res['b_price']; ?>" onkeypress='return event.charCode >= 48 && event.charCode <= 57' required ></td>
    </tr>
  
  </table>
  <br>
  <button type="submit" name="done">ยืนยัน</button><br>
</form>
<?php 
if(isset($_POST['done'])){
	$bid = $_POST['nid'];
	$name = $_POST['name'];
	$pic = $_POST['pic'];
	$author = $_POST['author'];
	$year = $_POST['year'];
	$type = $_POST['type'];
	$detail = $_POST['detail'];
	$price = $_POST['price'];
	$sql = "UPDATE book SET b_id='$bid', b_name='$name', b_pic='$pic' , b_author='$author', b_year='$year', b_type='$type', b_detail='$detail' , b_price='$price'  where b_id='$id' ";
	$query = mysqli_query($conn,$sql);
	header('location:book_stock.php');
}
?>

</body>
</html>
<?php ob_end_flush() ?>