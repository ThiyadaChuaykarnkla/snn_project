<?php
session_start();
?>
<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
	<link rel="preconnect" href="https://fonts.gstatic.com">
	<link href="https://fonts.googleapis.com/css2?family=Taviraj:wght@100&display=swap" rel="stylesheet">

    <title>Payment</title>
</head>

<body style="background-color:#FFE8D2">


	<div class="container ">
		<div class="jumbotron" style="background-color:#F08879">
			<Font face='Taviraj', serif;>
				<center>
					<h1 style="color:#FFFFFF"> SNN-BOOK </h1>
				</center>
		</div>
	</div>

	<form action="" method="GET">
		<div class="row">
			<div class="col-sm-4">
				<center> ค้นหา : <input type="text" name="search" placeholder="กรอกชื่อหนังสือ"> <input type="submit" name="submit" value="ยืนยัน"> </center>
			</div>

			<div class="col">
				<link rel="stylesheet" type="text/css" href="styles.css">
				<div class="dropdown">
					<button class="dropbtn">ประเภทหนังสือ</button>
					<div class="dropdown-content">
						<a href="book_general_member.php">หมวดทั่วไป</a>
						<a href="book_cartoon_member.php">หมวดการ์ตูน</a>
						<a href="book_documentary_member.php">หมวดสารคดี</a>
						<a href="book_magazine_member.php">หมวดวารสาร</a>
						<a href="book_novel_member.php">หมวดนวนิยาย</a>
					</div>
				</div>
	</form>
	</div>
	<div class="col">
		<center> <button class="btn"><a href="context_host.php">ติดต่อเรา</a></button></center>
	</div>
	<div class="col">
		<left> <?php
					if ($_SESSION["name"]) {
					?>
				<p>ยินดีต้อนรับคุณ <?php echo $_SESSION["name"]; ?>.</p>
				<?php
					} else echo "<h1>กรุณาลงชื่อเข้าสู่ระบบก่อน .</h1>";
				?> </left>
	</div>
	
	
	<div class="col">
		<center> <button class="btn"><a href="index.php">ออกจากระบบ</a></button></center>
	</div>
	</div> <br><br>

        <?php
        include "connect_database.php";
        $id_book = $_GET['id'];
        $sql = "SELECT * FROM Book where b_id = $id_book";
        $query = mysqli_query($conn, $sql);
        while ($res = mysqli_fetch_array($query)) {
        ?>

            <table align="center" border="0" width="80%">
                <tr>
                    <td rowspan="3" align="center"> <img src="img/<?php echo $res['b_pic']; ?>" width="200" height="200"> </td>
                    <td> รหัสหนังสือ : <?php echo $res['b_id']; ?> </td>
                </tr>
                <tr>
                    <td> ชื่อหนังสือ : <?php echo $res['b_name']; ?></td>
                </tr>
                <tr>
                    <td> ราคา : <?php echo $res['b_price']; ?> บาท </td>
                </tr>
            </table>

            <br>
            <center>
            <button class="btn-primary btn"> <a href="book_payment.php?id=<?php echo $res['b_id']; ?>" class="text-white"> ย้อนกลับ </a> </button>
            <button class="btn-primary btn"> <a href="book_delivery.php?id=<?php echo $res['b_id']; ?>" class="text-white"> ยืนยันการชำระเงิน</a> </button>
            </center>

        <?php
        }
        ?>


</body>

</html>